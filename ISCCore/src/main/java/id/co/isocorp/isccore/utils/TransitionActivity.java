package id.co.isocorp.isccore.utils;

import android.app.Activity;
import android.content.Intent;

import id.co.isocorp.isccore.R;


/**
 * Created by Dirga on 10/10/2015.
 */
public class TransitionActivity {
    public static void transitionRightToLeft(Activity context, Intent intent) {
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_in_left_lib, R.anim.slide_out_left_lib);
    }

    public static void transitionDownToUp(Activity context, Intent intent) {
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_in_up_lib, R.anim.slide_out_up_lib);
    }

    public static void transitionRightToLeft(Activity context, Intent intent, int requestCode) {
        context.startActivityForResult(intent, requestCode);
        context.overridePendingTransition(R.anim.slide_in_left_lib, R.anim.slide_out_left_lib);
    }

    public static void transitionDownToUp(Activity context, Intent intent, int requestCode) {
        context.startActivityForResult(intent, requestCode);
        context.overridePendingTransition(R.anim.slide_in_up_lib, R.anim.slide_out_up_lib);
    }

    public static void transitionUpToDown(Activity context,Intent intent, int requestCode) {
        context.startActivityForResult(intent, requestCode);
        context.overridePendingTransition(R.anim.slide_in_down_lib, R.anim.slide_out_down_lib);
    }

    public static void transitionUpToDown(Activity context) {
        context.overridePendingTransition(R.anim.slide_in_down_lib, R.anim.slide_out_down_lib);
    }

    public static void transitionLeftToRight(Activity context) {
        context.overridePendingTransition(R.anim.slide_in_right_lib, R.anim.slide_out_right_lib);
    }

    public static void transitionLeftToRight(Activity context, Intent intent) {
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_in_right_lib, R.anim.slide_out_right_lib);
    }

    public static void transitionLeftToRight(Activity context, Intent intent, int requestCode) {
        context.startActivityForResult(intent, requestCode);
        context.overridePendingTransition(R.anim.slide_in_right_lib, R.anim.slide_out_right_lib);
    }
}
