package id.co.isocorp.isccore.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import id.co.isocorp.isccore.R;
import id.co.isocorp.isccore.utils.TypeFacePool;


/**
 * Created by Dirga on 08/03/2016.
 */
public class ISCEditText extends EditText {
    private String fontName;
    private AttributeSet attrs;

    public ISCEditText(Context context) {
        super(context);
    }

    public ISCEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;
        init();
    }

    public ISCEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.attrs = attrs;
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.ISCEditText, 0, 0);
            fontName = typedArray.getString(R.styleable.ISCEditText_edittext_font_lib);
            if (fontName != null) {
                setFontName(fontName);
                setFontType();
            }
        }
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    private void setFontType() {
        Typeface typeface = TypeFacePool.getTypeface(getContext(), "fonts/" + fontName);
        setTypeface(typeface);
    }
}
