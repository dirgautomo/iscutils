package id.co.isocorp.isccore.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by Dirga on 08/03/2016.
 */
public class TypeFacePool {
    private static final Map<String, WeakReference<Typeface>> typefaceMap = new WeakHashMap<>();

    public static Typeface getTypeface(Context context, String name) {
        WeakReference<Typeface> typeface = typefaceMap.get(name);
        if (typeface == null) {
            typeface = new WeakReference<>(Typeface.createFromAsset(context.getAssets(), name));
            typefaceMap.put(name, typeface);
        }
        return typeface.get();
    }

    public static void releaseMemory() {
        typefaceMap.clear();
    }
}
